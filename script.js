import { config } from "dotenv";
import { Configuration, OpenAIApi } from "openai";
import puppeteer from "puppeteer";
import express from "express";
import cors from "cors";
// Load environment variables from .env file
config();

// Initialize OpenAI API client
const openai = new OpenAIApi(
  new Configuration({
    apiKey: process.env.API_KEY,
  })
);

// node api integration
const app = express();
const port = process.env.PORT || 8000;

app.use(express.json());
app.use(cors());
app.post("/getXpath", async (req, res) => {
  if (!req.body.url || !req.body.fields) {
    res.status(400).send("Please provide the url and fields");
  } else {
    // Getting the dom values based on user input
    let chunks = [];

    // async function getDom() {
    //   // Launch a headless browser instance
    //   const browser = await puppeteer.launch();

    //   // Open a new page in the browser
    //   const page = await browser.newPage();

    //   // Navigate to the website URL
    //   const url = req.body.url;

    //   await page.goto(url);

    //   // Extract the DOM tree from the rendered HTML content
    //   const dom = await page.evaluate(() => {
    //     // Remove all script tags from the document
    //     const scripts = document.getElementsByTagName("script");
    //     const metaTags = document.getElementsByTagName("meta");
    //     const styleTags = document.getElementsByTagName("style");
    //     const linkTags = document.getElementsByTagName("link");

    //     for (let i = styleTags.length - 1; i >= 0; i--) {
    //       styleTags[i].parentNode.removeChild(styleTags[i]);
    //     }
    //     for (let i = linkTags.length - 1; i >= 0; i--) {
    //       linkTags[i].parentNode.removeChild(linkTags[i]);
    //     }
    //     for (let i = scripts.length - 1; i >= 0; i--) {
    //       scripts[i].parentNode.removeChild(scripts[i]);
    //     }
    //     for (let i = metaTags.length - 1; i >= 0; i--) {
    //       metaTags[i].parentNode.removeChild(metaTags[i]);
    //     }

    //     return document.documentElement.outerHTML;
    //   });

    //   // Split the DOM tree into chunks of 1000 characters
    //   const chunkSize = 1000;
    //   for (let i = 0; i < dom.length; i += chunkSize) {
    //     chunks.push(dom.substring(i, i + chunkSize));
    //   }

    //   // Close the browser instance
    //   await browser.close();
    //   // Log the result string to the console
    //   return chunks;
    // }

    // console.log(await getDom());

    const xpathDomElement = req.body.fields;

    // Define message to send to GPT-3 chat
    const message = `Write the effective xpath for ${xpathDomElement} from below dom code: `;
    // const data = await getDom();

    // console.log("data", data);
    let finalData = [];

    const abc = {
      elementType: "input",
      keyword: "_token",
      xpath: '//form[@id="login"]//input[1]',
    };
    const resp = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [
        {
          role: "user",
          content: `can you please write as pageobject struture for playwright ${abc}`,
        },
      ],
    });

    console.log("Please check here...", resp.data.choices[0].message.content);

    // await Promise.all(
    //   data.map(async (item) => {
    //     // Send message to GPT-3 chat
    //     try {
    //       const resp = await openai.createChatCompletion({
    //         model: "gpt-3.5-turbo",
    //         messages: [{ role: "user", content: `${message}:  ${item}` }],
    //       });
    //       finalData.push(resp.data.choices[0].message.content);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //   })
    // );
    res.send(finalData);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
