import puppeteer from "puppeteer";
import { Configuration, OpenAIApi } from "openai";
import { config } from "dotenv";
import express from "express";
import cors from "cors";

config();

// Initialize OpenAI API client
const openai = new OpenAIApi(
  new Configuration({
    apiKey: process.env.API_KEY,
  })
);

// node api integration
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

async function extractKeywordsFromWebsite(url) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle0" });

  const keywords = await page.evaluate(() => {
    function getXPathForElement(element) {
      const xpath = [];
      while (element && element.nodeType === Node.ELEMENT_NODE) {
        let part = element.nodeName.toLowerCase();
        if (element.id) {
          part += `[@id="${element.id}"]`;
          xpath.unshift(part);
          break;
        } else {
          let siblings = element.parentNode.childNodes;
          let count = 0;
          for (let i = 0; i < siblings.length; i++) {
            let sibling = siblings[i];
            if (sibling.nodeName === element.nodeName) {
              count++;
            }
            if (sibling === element) {
              part += `[${count}]`;
              xpath.unshift(part);
              break;
            }
          }
          element = element.parentNode;
        }
      }
      return xpath.length ? `//${xpath.join("//")}` : null;
    }

    const interactiveElements = Array.from(
      document.querySelectorAll("input, select, textarea")
    );
    const keywords = [];
    interactiveElements.forEach((element) => {
      const elementType = element.tagName.toLowerCase();
      const keyword =
        element.getAttribute("name") || element.getAttribute("id") || "";
      const xpath = getXPathForElement(element).replace(/\\/g, "");
      keywords.push({ elementType, keyword, xpath });
    });
    return keywords;
  });

  const jsonFile = JSON.stringify(keywords);
  const resp = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: [
      {
        role: "user",
        content: `can you please write as pageobject struture for playwright which only returns JS class file, provide only code lines in to JS class file and remove description. ${jsonFile}`,
      },
    ],
  });

  await browser.close();
  return resp.data.choices[0].message.content;
}

app.post("/getXpath", async (req, res) => {
  if (!req.body.url) {
    res.status(400).send("Please provide the url and fields");
  } else {
    // Example usage
    const url = req.body.url;
    try {
      const data = await extractKeywordsFromWebsite(url);
      res.send(data);
    } catch (error) {
      res.send(error);
      console.log(error);
    }
  }
});


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });
  