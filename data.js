class PlaywrightPage {

  constructor(page) {
    this.page = page;
  }
  
  async _token() {
    return await this.page.$('//form[@id="login"]//input[1]');
  }
  
  async url_data() {
    return await this.page.$('//input[@id="url_data"]');
  }
  
  async afterHash() {
    return await this.page.$('//input[@id="afterHash"]');
  }
  
  async email_id() {
    return await this.page.$('//input[@id="email_id"]');
  }
  
  async password() {
    return await this.page.$('//input[@id="password"]');
  }

  async customRadioInline1() {
    return await this.page.$('//input[@id="customRadioInline1"]');
  }
  
  async customRadioInline2() {
    return await this.page.$('//input[@id="customRadioInline2"]');
  }
  
  async sign_up_token() {
    return await this.page.$('//form[@id="sign_up"]//input[1]');
  }
  
  async sign_up_url_data() {
    return await this.page.$('//input[@id="url_data"]');
  }
  
  async trial_type() {
    return await this.page.$('//input[@id="trial_type"]');
  }
  
  async plan_id() {
    return await this.page.$('//input[@id="plan_id"]');
  }
  
  async month() {
    return await this.page.$('//input[@id="month"]');
  }
  
  async year() {
    return await this.page.$('//input[@id="year"]');
  }
  
  async payment_method_nonce() {
    return await this.page.$('//input[@id="nonce"]');
  }
  
  async first_name() {
    return await this.page.$('//input[@id="first_name"]');
  }
  
  async last_name() {
    return await this.page.$('//input[@id="last_name"]');
  }

  async username() {
    return await this.page.$('//input[@id="username"]');
  }
  
  async dob() {
    return await this.page.$('//input[@id="dob"]');
  }
  
  async country_code() {
    return await this.page.$('//form[@id="sign_up"]//div[7]//div[1]//select[1]');
  }
  
  async phone_no() {
    return await this.page.$('//input[@id="phone_no"]');
  }
  
  async password_v1() {
    return await this.page.$('//input[@id="password_v1"]');
  }
  
  async confirm_password() {
    return await this.page.$('//input[@id="confirm_password"]');
  }
  
  async about_us() {
    return await this.page.$('//textarea[@id="about_us"]');
  }
  
  async address1() {
    return await this.page.$('//input[@id="address1"]');
  }
  
  async address2() {
    return await this.page.$('//input[@id="address2"]');
  }
  
  async city() {
    return await this.page.$('//input[@id="city"]');
  }
  
  async state() {
    return await this.page.$('//input[@id="state"]');
  }
  
  async postal_code() {
    return await this.page.$('//input[@id="postal_code"]');
  }
  
  async billing_country() {
    return await this.page.$('//div[@id="billing-info-hide"]//div[6]//select[1]');
  }
  
  async cardholder_name() {
    return await this.page.$('//input[@id="cardholder_name"]');
  }
  
  async card_number() {
    return await this.page.$('//input[@id="card_number"]');
  }
  
  async exp_date() {
    return await this.page.$('//input[@id="exp_date"]');
  }
  
  async sec_code() {
    return await this.page.$('//input[@id="sec_code"]');
  }

  async payment() {
    return await this.page.$('//input[@id="checkboxmodal"]');
  }
  
  async g_recaptcha_response() {
    return await this.page.$('//textarea[@id="g-recaptcha-response"]');
  }
  
  async terms() {
    return await this.page.$('//input[@id="terms"]');
  }
  
  async notification() {
    return await this.page.$('//input[@id="notification"]');
  }
  
  async promo_code() {
    return await this.page.$('//input[@id="promo-code-model"]');
  }
}

module.exports = PlaywrightPage;